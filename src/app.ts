import express from "express";
import compression from "compression";
import bodyParser from "body-parser";
import helmet from "helmet";
import mongoose from "mongoose";
import Logger from "./core/Logger";
import notFoundHandler from "./middleware/NotFoundHandler";
import errorHandler from "./middleware/ErrorHandler";
import { port, mongodbUri } from "./config";

// Controllers (route handlers)
import * as tinyUrlsController from "./controllers/TinyUrlsController";

// Create Express server
const app = express();

// Connect to MongoDB
mongoose.connect(mongodbUri(true), {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    autoIndex: true,
    poolSize: 10,            // Maintain up to 10 socket connections
    bufferMaxEntries: 0,     // If not connected, return errors immediately rather than waiting for reconnect
    connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
    socketTimeoutMS: 45000,  // Close sockets after 45 seconds of inactivity
}).then(() => {
    /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
}).catch(err => {
    Logger.error("MongoDB connection error. Please make sure MongoDB is running. " + err);
    // process.exit();
});

// Express configuration
app.set("port", port || "8080");
app.use(helmet());
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Primary app routes.
app.get("/[a-zA-Z0-9]{9}", tinyUrlsController.follow);

// API routes.
app.post("/api/v1/tinyurls", tinyUrlsController.create);
app.get("/api/v1/tinyurls", tinyUrlsController.getAll);
app.get("/api/v1/tinyurls/:id", tinyUrlsController.getOne);

// 404 handler
app.use(notFoundHandler);

// Error Handler
app.use(errorHandler);

export default app;