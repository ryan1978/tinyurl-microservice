import "dotenv/config";
import Logger from "./core/Logger";
import app from "./app";

/**
 * Start Express server.
 */
const server = app
    .listen(app.get("port"), () => {
        Logger.info(`App is running at http://localhost:${app.get("port")} in ${app.get("env")} mode`);
        Logger.info("Press CTRL-C to stop");
    }).on("error", err => {
        Logger.error(err);
    });

export default server;